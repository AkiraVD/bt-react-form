export const ADD_SV = "ADD_SV";
export const DEL_SV = "DEL_SV";
export const EDIT_SV = "EDIT_SV";
export const CHANGE_SV = "CHANGE_SV";

export const BLANK_ERR = "Không được để trống";
export const DUPLICATE_ERR = "Đã tồn tại";
export const NAN_ERR = "Chỉ được chứa số";
export const FORMAT_ERR = "Chưa đúng định dạng";
