import { ADD_SV, CHANGE_SV, DEL_SV, EDIT_SV } from "../constant/formConstant";

let innitialState = {
  listSinhVien: [],
  isEditing: false,
  editIndex: null,
  SVEdit: {},
};

export const formReducer = (state = innitialState, action) => {
  switch (action.type) {
    case ADD_SV: {
      let cloneArray = [...state.listSinhVien];
      cloneArray.push(action.payload);
      return { ...state, listSinhVien: cloneArray };
    }
    case DEL_SV: {
      let cloneArray = [...state.listSinhVien];
      cloneArray.splice(action.payload, 1);
      return { ...state, listSinhVien: cloneArray };
    }
    case EDIT_SV: {
      let cloneArray = [...state.listSinhVien];
      let SVEdit = cloneArray[action.payload];
      let editIndex = action.payload;
      return { ...state, SVEdit, editIndex, isEditing: true };
    }
    case CHANGE_SV: {
      let cloneArray = [...state.listSinhVien];
      cloneArray[state.editIndex] = action.payload;
      return {
        ...state,
        listSinhVien: cloneArray,
        isEditing: false,
        editIndex: null,
        SVEdit: {},
      };
    }
    default:
      return state;
  }
};
