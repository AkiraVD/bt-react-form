import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ADD_SV,
  BLANK_ERR,
  CHANGE_SV,
  DUPLICATE_ERR,
  FORMAT_ERR,
  NAN_ERR,
} from "../constant/formConstant";

class FormInput extends Component {
  state = {
    newSV: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    validation: {
      idErr: "",
      nameErr: "",
      phoneErr: "",
      emailErr: "",
    },
  };
  componentWillReceiveProps(nextProps, nextContext) {
    console.log("nextProps: ", nextProps);
    if (nextProps.isEditing) {
      this.setState({
        newSV: {
          id: nextProps.SVEdit.id,
          name: nextProps.SVEdit.name,
          phone: nextProps.SVEdit.phone,
          email: nextProps.SVEdit.email,
        },
      });
    }
  }
  handleGetUserForm = (event) => {
    let { name, value } = event.target;
    let newSV = { ...this.state.newSV, [name]: value };
    this.setState({ newSV });
  };
  handleValidation = () => {
    let { id, name, phone, email } = this.state.newSV;
    let validationMsg = this.state.validation;
    let isValid = true;

    // Validation ID
    let idIndex = this.props.listSinhVien.findIndex((value) => value.id === id);
    if (id === "") {
      validationMsg.idErr = BLANK_ERR;
      isValid = isValid & false;
    } else if (this.props.isEditing === false && idIndex !== -1) {
      validationMsg.idErr = DUPLICATE_ERR;
      isValid = isValid & false;
    } else {
      validationMsg.idErr = "";
    }

    // Validate name
    if (name === "") {
      validationMsg.nameErr = BLANK_ERR;
      isValid = isValid & false;
    } else {
      validationMsg.nameErr = "";
    }

    // Validate phone
    if (phone === "") {
      validationMsg.phoneErr = BLANK_ERR;
      isValid = isValid & false;
    } else if (isNaN(phone)) {
      validationMsg.phoneErr = NAN_ERR;
      isValid = isValid & false;
    } else {
      validationMsg.phoneErr = "";
    }

    // Validate email
    let mailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (email === "") {
      validationMsg.emailErr = BLANK_ERR;
      isValid = isValid & false;
    } else if (!mailRegex.test(email)) {
      validationMsg.emailErr = FORMAT_ERR;
      isValid = isValid & false;
    } else {
      validationMsg.emailErr = "";
    }

    this.setState({ validationMsg: validationMsg });
    return isValid;
  };
  render() {
    return (
      <div className="container">
        <div className="bg-dark text-white text-start py-1 px-3">
          <h2>Thông tin sinh viên</h2>
        </div>
        <div className="row">
          <div className="col-sm-6 mt-2">
            <div>
              <p className="text-start mb-0">
                Mã SV{" "}
                <span className="text-danger">
                  {this.state.validation.idErr}
                </span>
              </p>
              <input
                disabled={this.props.isEditing}
                type="text"
                className="form-control"
                name="id"
                placeholder="Nhập mã SV"
                value={this.state.newSV.id}
                onChange={this.handleGetUserForm}
                ref={this.inputRef}
              />
            </div>
          </div>
          <div className="col-sm-6 mt-2">
            <div>
              <p className="text-start mb-0">
                Họ Tên{" "}
                <span className="text-danger">
                  {this.state.validation.nameErr}
                </span>
              </p>
              <input
                type="text"
                className="form-control"
                name="name"
                placeholder="VD: Nguyễn Văn A"
                value={this.state.newSV.name}
                onChange={this.handleGetUserForm}
              />
            </div>
          </div>
          <div className="col-sm-6 mt-2">
            {" "}
            <div>
              <p className="text-start mb-0">
                Số điện thoại{" "}
                <span className="text-danger">
                  {this.state.validation.phoneErr}
                </span>
              </p>
              <input
                type="text"
                className="form-control"
                name="phone"
                placeholder="VD: 0901234567"
                value={this.state.newSV.phone}
                onChange={this.handleGetUserForm}
              />
            </div>
          </div>
          <div className="col-sm-6 mt-2">
            {" "}
            <div>
              <p className="text-start mb-0">
                Email{" "}
                <span className="text-danger">
                  {this.state.validation.emailErr}
                </span>
              </p>
              <input
                type="text"
                className="form-control"
                name="email"
                placeholder="VD: abc@mail.com"
                value={this.state.newSV.email}
                onChange={this.handleGetUserForm}
              />
            </div>
          </div>
        </div>
        <button
          className="btn btn-success mt-2"
          disabled={this.props.isEditing}
          onClick={() => {
            this.handleValidation() &&
              this.props.handleSubmitForm(this.state.newSV);
          }}
        >
          Thêm sinh viên
        </button>
        <button
          disabled={!this.props.isEditing}
          className="btn btn-warning mt-2 ms-2"
          onClick={() => {
            this.handleValidation() &&
              this.props.handleChange(this.state.newSV);
          }}
        >
          Thay đổi sinh viên
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listSinhVien: state.formReducer.listSinhVien,
    isEditing: state.formReducer.isEditing,
    SVEdit: state.formReducer.SVEdit,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleSubmitForm: (newSV) => {
      dispatch({
        type: ADD_SV,
        payload: newSV,
      });
    },
    handleChange: (newSV) => {
      dispatch({
        type: CHANGE_SV,
        payload: newSV,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormInput);
