import React, { Component } from "react";
import { connect } from "react-redux";
import { DEL_SV, EDIT_SV } from "../constant/formConstant";

class FormTable extends Component {
  handleRenderTable = () => {
    return this.props.listSinhVien.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.phone}</td>
          <td>{item.email}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDelete(index);
              }}
            >
              Xóa
            </button>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleEdit(index);
              }}
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container mt-3">
        <table className="table">
          <thead className="bg-dark text-white">
            <tr>
              <th>Mã SV</th>
              <th>Họ Tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Thao Tác</th>
            </tr>
          </thead>
          <tbody>{this.handleRenderTable()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    listSinhVien: state.formReducer.listSinhVien,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (index) =>
      dispatch({
        type: DEL_SV,
        payload: index,
      }),
    handleEdit: (index) =>
      dispatch({
        type: EDIT_SV,
        payload: index,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormTable);
