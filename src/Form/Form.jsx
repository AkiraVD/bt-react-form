import React, { Component } from "react";
import FormInput from "./FormInput";
import FormTable from "./FormTable";

export default class Form extends Component {
  render() {
    return (
      <div className="py-5">
        <FormInput />
        <FormTable />
      </div>
    );
  }
}
